package com.workshop.randomgifs;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

  private Gif currentGif;
  private GetRandomGif getRandomGif = new GetRandomGif(new GifRepository());

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  private Gif getRandomGif() {
    currentGif = getRandomGif.run();
    return currentGif;
  }

  private void setRandomGif() {
    // What will be added here?
  }

  private void navigateToShareActivity() {
    // What will be added here?
  }
}
