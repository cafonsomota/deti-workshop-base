# Workshop: Let’s dive into Android (and build your first application) 🏗

With more than 2.5 billion users actively using their Android smartphones daily on a range of products that go from your smartphone, tablet, watch and even your car - how amazing it would be you if you were walking down the street and just see someone using an application that you've built?

Well, this is the first step to it! Let’s start small and build your first Android application - and yes, of course, it will be done on Kotlin! 


## Setup (for Android Studio)

1. [Download and install](https://developer.android.com/sdk/installing/index.html?pkg=studio) Android Studio
2. Clone this repository
3. Run the project